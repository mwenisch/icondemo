//
//  SnapViewController.m
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import "SnapViewController.h"

@interface SnapViewController ()

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UISnapBehavior *snapBehavior;

@end

@implementation SnapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.snapBehavior = [[UISnapBehavior alloc] initWithItem:self.testView
                                                 snapToPoint:self.testView.center];
    [self.animator addBehavior:self.snapBehavior];
    
//    UIDynamicItemBehavior *rotation = [[UIDynamicItemBehavior alloc] initWithItems:@[self.testView]];
//    rotation.allowsRotation = NO;
//    [self.animator addBehavior:rotation];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTaped:)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)userTaped:(UITapGestureRecognizer *)gesture
{
    CGPoint tapPoint = [gesture locationInView:self.view];
    
    [self.animator removeBehavior:self.snapBehavior];
    
    self.snapBehavior = [[UISnapBehavior alloc] initWithItem:self.testView
                                                 snapToPoint:tapPoint];
    self.snapBehavior.damping = 0.4;
    [self.animator addBehavior:self.snapBehavior];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
