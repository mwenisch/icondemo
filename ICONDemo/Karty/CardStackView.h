//
//  CardStackView.h
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"

@interface CardStackView : UIView <UICollisionBehaviorDelegate, UIDynamicAnimatorDelegate>

@property (nonatomic, strong) NSMutableArray *panGestures;

@property (nonatomic, strong) NSMutableArray *cardViewControllers;

@end
