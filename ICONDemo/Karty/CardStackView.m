//
//  CardStackView.m
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import "CardStackView.h"

@interface CardStackView ()

@property (strong, nonatomic) UIDynamicAnimator *animator;

@end

@implementation CardStackView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self];
        
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)configureAnimatorForView:(CardView *)view
{
    view.pushBehavior = [[UIPushBehavior alloc] initWithItems:@[view]
                                                         mode:UIPushBehaviorModeInstantaneous];
    
    view.collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[view]];
    view.collisionBehavior.collisionDelegate = self;
    [self.animator addBehavior:view.collisionBehavior];
    
    CGPoint center = view.center;
    //center.x = center.x - 4;
    
    view.snapBehavior = [[UISnapBehavior alloc] initWithItem:view
                                                 snapToPoint:center];
    view.snapBehavior.damping = 0.95;
    
    //add pan gesture
    self.panGestures = [[NSMutableArray alloc] init];
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [view addGestureRecognizer:panGesture];
    
    [self.panGestures addObject:panGesture];
    
    CGFloat inset = -hypot(self.animator.referenceView.bounds.size.width * 2.0, self.animator.referenceView.bounds.size.height * 2.0);
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(inset, inset, inset, inset);
    [view.collisionBehavior setTranslatesReferenceBoundsIntoBoundaryWithInsets:edgeInsets];
}

- (void)handleGestureBegin:(UIPanGestureRecognizer*)gestureRecognizer {
    CardView *gestureView = (CardView *)gestureRecognizer.view;
    
    CGPoint anchorPoint = [gestureRecognizer locationInView:self];
    CGPoint pointInSuperView = [gestureRecognizer locationInView:gestureView];
    UIOffset offset = UIOffsetMake(pointInSuperView.x - gestureView.bounds.size.width / 2, pointInSuperView.y - gestureView.bounds.size.height / 2);
    UIAttachmentBehavior* attachment = [[UIAttachmentBehavior alloc] initWithItem:gestureView
                                                                 offsetFromCenter:offset
                                                                 attachedToAnchor:anchorPoint];
    gestureView.attachmentBehavior = attachment;
    [self.animator addBehavior:attachment];
    
    [self.animator removeBehavior:gestureView.snapBehavior];
}

- (void)handleGestureMoved:(UIPanGestureRecognizer*)gestureRecognizer {
    CardView *gestureView = (CardView *)gestureRecognizer.view;
    
    CGPoint anchorPoint = [gestureRecognizer locationInView:self];
    gestureView.attachmentBehavior.anchorPoint = anchorPoint;
}

- (void)handleGestureEnd:(UIPanGestureRecognizer*)gestureRecognizer {
    CardView *gestureView = (CardView *)gestureRecognizer.view;
    
    [self.animator removeBehavior:gestureView.attachmentBehavior];
    
    CGPoint velocity = [gestureRecognizer velocityInView:self];
    CGFloat velocityMagnitude = hypot(velocity.x, velocity.y);
    if (velocityMagnitude > 1000) {
        velocityMagnitude = 1000;
    }
    
    if (velocityMagnitude<500) {
        [self.animator addBehavior:gestureView.snapBehavior];
    } else {
        
        CGPoint touchLocation = [gestureRecognizer locationInView:self];
        UIOffset offset = [self toc_centerOffsetForPoint:touchLocation];
        
        gestureView.pushBehavior = [[UIPushBehavior alloc] initWithItems:@[gestureView]
                                                                    mode:UIPushBehaviorModeInstantaneous];
        [gestureView.pushBehavior setTargetOffsetFromCenter:offset
                                                    forItem:self];
        gestureView.pushBehavior.pushDirection = [self toc_forceFromVelocity:velocity];
        gestureView.pushBehavior.active = YES;
        [self.animator addBehavior:gestureView.pushBehavior];
    }
    
}

-(void)handlePanGesture:(UIPanGestureRecognizer*)gestureRecognizer {
    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan:
            [self handleGestureBegin:gestureRecognizer];
            break;
        case UIGestureRecognizerStateChanged:
            [self handleGestureMoved:gestureRecognizer];
            break;
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateFailed:
            [self handleGestureEnd:gestureRecognizer];
            break;
        default:
            break;
    }
}

- (UIOffset)toc_centerOffsetForPoint:(CGPoint)point {
    CGRect bounds = self.bounds;
    return UIOffsetMake(point.x-CGRectGetMidX(bounds), point.y-CGRectGetMidY(bounds));
}

- (CGVector)toc_forceFromVelocity:(CGPoint)velocity {
    return [self toc_forceFromVelocity:velocity withDensity:1.0];
}
- (CGVector)toc_forceFromVelocity:(CGPoint)velocity withDensity:(CGFloat)density{
    CGRect bounds = self.bounds;
    CGFloat area = CGRectGetWidth(bounds)*CGRectGetHeight(bounds);
    const CGFloat UIKitNewtonScaling = 1000000.0;
    CGFloat scaling = density*area/UIKitNewtonScaling;
    
    return CGVectorMake(velocity.x*scaling, velocity.y*scaling);
}

- (void)shiftCard:(UIView *)view
{
    for (int i = 0; i < self.cardViewControllers.count; i++) {
        if ([self.cardViewControllers[i] view] == view) {
            id object = [self.cardViewControllers objectAtIndex:i];
            [self.cardViewControllers removeObjectAtIndex:i];
            [self.cardViewControllers insertObject:object atIndex:0];
        }
    }
    
    for (int i = 1; i < self.cardViewControllers.count; i++) {
        [self bringSubviewToFront:[(UIViewController *)self.cardViewControllers[i] view]];
    }
    
}

- (void)setCardViewControllers:(NSMutableArray *)cardViewControllers
{
    _cardViewControllers = cardViewControllers;
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    [self.animator removeAllBehaviors];
    
    for (UIViewController *controller in cardViewControllers) {
        [self addSubview:controller.view];
        [self configureAnimatorForView:(CardView *)controller.view];
    }
}

#pragma mark - UICollisionBehaviorDelegate
- (void) collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier atPoint:(CGPoint)p {
    [self.animator removeBehavior:((CardView *)item).pushBehavior];
    [self.animator addBehavior:((CardView *)item).snapBehavior];
    [self shiftCard:(UIView *)item];
}

@end
