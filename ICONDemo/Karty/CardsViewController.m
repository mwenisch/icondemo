//
//  CardsViewController.m
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import "CardsViewController.h"
#import "TestCardViewController.h"

@interface CardsViewController ()

@property (nonatomic, strong) IBOutlet CardStackView *testStack;

@end

@implementation CardsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
    NSMutableArray *testCards = [[NSMutableArray alloc] init];
    for (int i = 0; i < 5; i++) {
        TestCardViewController *testCard = [[TestCardViewController alloc] init];
        testCard.view.frame = CGRectMake(0, 0, 300, 160);
        testCard.numberLabel.text = [NSString stringWithFormat:@"%i", i + 1];
        [testCards addObject:testCard];
    }
    self.testStack.cardViewControllers = testCards;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
