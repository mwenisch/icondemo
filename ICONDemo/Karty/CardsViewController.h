//
//  CardsViewController.h
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardStackView.h"

@interface CardsViewController : UIViewController

@end
