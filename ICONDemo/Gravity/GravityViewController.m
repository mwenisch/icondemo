//
//  GravityViewController.m
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import "GravityViewController.h"

@interface GravityViewController ()

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIGravityBehavior *gravityBehavior;
@property (nonatomic, strong) UICollisionBehavior *collisionBehavior;

@end

@implementation GravityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    [self setupGravity];
}

- (void)setupGravity
{
    NSMutableArray *dynamicItems = [[NSMutableArray alloc] initWithObjects:self.testView, nil];
    
    self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:dynamicItems];
    self.gravityBehavior.angle = M_PI_2;
    self.gravityBehavior.magnitude = 10.0;
    [self.animator addBehavior:self.gravityBehavior];
    
    __weak GravityViewController *weakSelf = self;
    [self.gravityBehavior setAction:^{
        NSLog(@"y position: %f", weakSelf.testView.frame.origin.y);
        NSLog(@"y presentation layer pos: %f", ((CALayer *)weakSelf.testView.layer.presentationLayer).frame.origin.y);

        if (weakSelf.testView.frame.origin.y > (weakSelf.view.bounds.size.height - 1 - weakSelf.testView.frame.size.height)) {
            NSLog(@"animation end callback??");
        }
    }];
    
    self.collisionBehavior = [[UICollisionBehavior alloc] initWithItems:dynamicItems];
    self.collisionBehavior.collisionMode = UICollisionBehaviorModeBoundaries;
    [self.collisionBehavior addBoundaryWithIdentifier:@"bottom" fromPoint:CGPointMake(0, self.view.bounds.size.height + 1) toPoint:CGPointMake(320, self.view.bounds.size.height + 1)];
    [self.animator addBehavior:self.collisionBehavior];
    
    UIDynamicItemBehavior *elasticBehavior = [[UIDynamicItemBehavior alloc] initWithItems:dynamicItems];
    elasticBehavior.elasticity = 0.7;
    [self.animator addBehavior:elasticBehavior];
}

- (void)restartButtonPressed:(id)sender
{
    [self.animator removeAllBehaviors];
    self.testView.frame = CGRectMake(110, 80, 100, 100);
    [self setupGravity];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
