//
//  PushViewController.m
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import "PushViewController.h"

@interface PushViewController ()

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIPushBehavior *pushBehavior;
@property (nonatomic, strong) UIDynamicItemBehavior *resistanceBehavior;

@end

@implementation PushViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushButtonPressed:(id)sender
{
    [self.animator removeAllBehaviors];
    self.testView.frame = CGRectMake(110, 360, 100, 100);
    
    self.pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.testView]
                                                                mode:UIPushBehaviorModeInstantaneous];
    self.pushBehavior.angle = 3 * M_PI_2;
    self.pushBehavior.magnitude = 5.0;
    self.pushBehavior.active = YES;
    [self.animator addBehavior:self.pushBehavior];
    
    self.resistanceBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.testView]];
    self.resistanceBehavior.resistance = 2.0;
    [self.animator addBehavior:self.resistanceBehavior];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
