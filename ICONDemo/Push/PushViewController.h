//
//  PushViewController.h
//  ICONDemo
//
//  Created by Martin Wenisch on 22/03/14.
//  Copyright (c) 2014 September Projects s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *testView;

- (IBAction)pushButtonPressed:(id)sender;

@end
